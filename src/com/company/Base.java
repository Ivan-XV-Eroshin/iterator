package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

abstract class Base implements Collection {

    List<Base> bases;
    String name;


    Base(String name) {
        this.name = name;
        bases = new ArrayList<>();
    }


    @Override
    public Iterator getIterator() {
        return new BaseIterator(this);
    }

    public List<Base> getBases() {
        return bases;
    }

    public void setBases(List<Base> bases) {
        this.bases = bases;
    }

    public void addChildren(List<Base> baseList) {
        bases.addAll(baseList);
    }

    String getName() {
        return name;
    }


    public void printAll() {
        System.out.println("    " + name);//
    }

    public void printBase() {
        System.out.println("name " + name + "\nhash code: " + hashCode());
    }

    public void deleteByName(String name) {
    }

    private class BaseIterator implements Iterator {
        Stack<BaseIterator> statesStack;
        Base root;
        Base current;
        Base parent;
        int currentIndex;
        int parentIndex;
        boolean hasChildren;
        boolean hasNeighbours;

        public boolean isHasNeighbours() {
            return hasNeighbours;
        }

        public Base getRoot() {
            return root;
        }

        public Base getCurrent() {
            return current;
        }

        public Base getParent() {
            return parent;
        }

        public int getCurrentIndex() {
            return currentIndex;
        }

        public int getParentIndex() {
            return parentIndex;
        }


        public BaseIterator(Base root, Base current, Base parent, int currentIndex, int parentIndex, boolean hasChildren, boolean hasNeighbours) {
            this.root = root;
            this.current = current;
            this.parent = parent;
            this.currentIndex = currentIndex;
            this.parentIndex = parentIndex;
            this.hasChildren = hasChildren;
        }

        BaseIterator(BaseIterator other){
            this(other.getRoot(),
                    other.getCurrent(),
                    other.getParent(),
                    other.getCurrentIndex(),
                    other.getParentIndex(),
                    other.getHasChildren(),
                    other.isHasNeighbours());
        }

        private boolean getHasChildren() {
            return hasChildren;
        }
        BaseIterator(Base root) {
            statesStack = new Stack<>();
            this.root = root;
            if (this.root.getBases().size() > 0){
              current = this.root;
              parent = this.root;
              currentIndex = 0;
            }
        }

        @Override
        public Base next() {
            if (hasNext()){
                if(hasChildren){

                    hasChildren = false;
                    BaseIterator persist = new BaseIterator(this);
                    statesStack.push(persist);
                    currentIndex = 0;

                    parent= current;
                    current = parent.getBases().get(currentIndex);

                } else if (hasNeighbours) {

                    hasNeighbours = false;
                    current = parent.getBases().get(currentIndex);

                } else if (statesStack.size() > 0) {

                    BaseIterator savedIterator = statesStack.pop();
                    currentIndex = savedIterator.currentIndex;
                    parent = savedIterator.getParent();
                    current = savedIterator.getCurrent();
                    hasNeighbours = savedIterator.isHasNeighbours();

                    if (current == root) {
                        currentIndex++;
                        return current;
                    }
                    if (parent.getBases().size() == currentIndex + 1){
                        return next();
                    }
                    currentIndex++;
                    if (current.getBases().size() > currentIndex) {

                        current = parent.getBases().get(currentIndex);
                    }

                }

                if(current.getBases().size() != 0){
                    hasChildren = true;
                } else if (parent.getBases().size()-1 > currentIndex) {
                    currentIndex++;
                    hasNeighbours = true;
                }

            }
            return current;
        }

        @Override
        public boolean hasNext() {
            return parent != root || currentIndex + 2 != root.getBases().size() || current != root;
        }
    }

}
