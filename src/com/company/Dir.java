package com.company;


import java.util.ArrayList;
import java.util.List;

public class Dir extends Base {


    Dir(String name, List<Base> baseList) {
        super(name);
        if (baseList == null)
            this.bases = new ArrayList<>();
        else
            bases = baseList;
    }

    @Override
    public void printAll() {
        System.out.println(this.name);
        System.out.println("    --------Inner Members--------");
        for (Base base : bases) {
            base.printAll();
        }
        System.out.println("    --------End of Inners--------");
    }

    private Base findByName(String name) {
        if (this.name.equals(name))
            return this;
        else
            for (Base base : bases) {
                if (base.getName().equals(name))
                    return base;
            }
        return null;
    }

    void addBase(Base base) {
        bases.add(base);
    }

    public void addBaseList(List<Base> baseList) {
        this.bases.addAll(baseList);
    }

    @Override
    public void deleteByName(String name) {
        for (Base base : bases) {
            base.deleteByName(name);
        }
        bases.remove(findByName(name));
    }
}
