package com.company;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<Base> linkerFiles = new ArrayList<>();
        {
            linkerFiles.add(new File("1st file"));
            linkerFiles.add(new File("2nd file"));
            linkerFiles.add(new File("3rd file"));
            linkerFiles.add(new File("4th file"));
            linkerFiles.add(new File("5th file"));
        }
        Dir linker = new Dir("linker Root", linkerFiles);

        System.out.println("\nВсе папки из директории для компоновщика:");
        linker.printAll();

        linker.deleteByName("1st file");
        linker.addBase(new File("Random Name"));

        System.out.println("\nВсе папки из директории для компоновщика после удаления:");
        linker.printAll();




        Dir directory = new Dir("Корневая Директория", null);
        {
            List<Base> childrenNodes = new ArrayList<>();

            childrenNodes.add(new Dir("--lvl 1u", null));
            childrenNodes.add(new Dir("--lvl 2u", null));
            childrenNodes.add(new Dir("--lvl 3u", null));
            directory.addBaseList(childrenNodes);
            childrenNodes.clear();
            childrenNodes.add(new Dir("----2lvl 1u", null));
            childrenNodes.add(new Dir("----2lvl 2u", null));
            childrenNodes.add(new Dir("----2lvl 3u", null));
            directory.getBases().get(0).setBases(childrenNodes);
            List<Base> bases = new ArrayList<>();
            bases.add(new Dir("----2lvl 4u", null));
            bases.add(new Dir("----2lvl 5u", null));
            bases.add(new Dir("----2lvl 6u", null));
            bases.add(new Dir("----2lvl 7u", null));
            bases.add(new Dir("----2lvl 8u", null));
            directory.getBases().get(2).setBases(bases);
        }
        Iterator iterator = directory.getIterator();

        System.out.println("\nВсе папки из директории для итератора:");
        directory.printAll();


        System.out.println("\nПроход итератором: ");
        while (iterator.hasNext()) {
            System.out.println(iterator.next().getName());
        }

    }
}
